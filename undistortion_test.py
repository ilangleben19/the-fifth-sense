import numpy as np
import cv2

side = "left"

orig = cv2.imread("./calibration_images/" + side + "/8.jpg", 1)

camera_distortion_info = np.load("undistortion_info.npy").tolist()
mtx, dist = camera_distortion_info["mtx"], camera_distortion_info["dist"]

h, w = orig.shape[:2]
newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))

undistorted = cv2.undistort(orig, mtx, dist, None, newcameramtx)

x, y, w, h = roi
undistorted = undistorted[y:y+h, x:x+w]
cv2.imwrite("calibration_result.jpg", undistorted)