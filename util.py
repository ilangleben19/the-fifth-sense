import os, requests, signal, sys

import config

def restart():
    os.execl(sys.executable, sys.executable, *sys.argv)

def get_page(url):
    try:
        page = requests.get(url)
        if 200 <= int(page.status_code) < 400:
            return page.text.strip()
        else:
            return False
    except requests.exceptions.ConnectionError:
        return False

def get_interruption_checker(manager, on_interrupt_callback):
    def check_interrupted():
        if manager.interrupted:
            on_interrupt_callback()
            sys.exit("INFO: Ctrl+C was pressed, shutting down!")
    return check_interrupted

class InterruptableRegion(object):
    def __init__(self, sig=signal.SIGINT):
        self.sig = sig
        self.interrupted = False
        self.released = False
        self.original_handler = None

    def __enter__(self):
        self._validate_region_start()
        self._store_signal_default_handler()

        def _signal_invoked_new_handler(signum, frame):
            self._release()
            self.interrupted = True

        signal.signal(self.sig, _signal_invoked_new_handler)

        return self

    def __exit__(self, type, value, tb):
        self._release()

    def _validate_region_start(self):
        if self.interrupted or self.released or self.original_handler:
            raise RuntimeError("An interruptable region can only be used once")

    def _release(self):
        if not self.released:
            self._reset_signal_default_handler()
            self.released = True

    def _store_signal_default_handler(self):
        self.original_handler = signal.getsignal(self.sig)

    def _reset_signal_default_handler(self):
        signal.signal(self.sig, self.original_handler)