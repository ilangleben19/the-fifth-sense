from picamera import PiCamera
from time import sleep
from fractions import Fraction

res = (1024, 768)

with PiCamera() as camera:
	camera.resolution = res
	camera.start_preview()
	sleep(2)
	camera.capture("images/exposure-test/unset.jpg")

sleep(2)

with PiCamera() as camera:
	camera.resolution = res
	camera.exposure_mode = "night"
	camera.start_preview()
	sleep(2)
	camera.capture("images/exposure-test/low-exposure.jpg")

sleep(2)

with PiCamera() as camera:
	camera.resolution = res
	camera.exposure_mode = "auto"
	camera.start_preview()
	sleep(2)
	camera.capture("images/exposure-test/auto-exposure.jpg")
