from picamera import PiCamera
from time import sleep
import datetime

print("Starting up camera at (1024,768)...")
start = datetime.datetime.now()
with PiCamera() as camera:
	camera.resolution = (1024, 768)
	end = datetime.datetime.now()
	print("Duration: " + str(end - start))
	
	print("Warming up camera for 2 seconds...")
	camera.start_preview()
	sleep(2)
	print("Camera warmed up!")
	
	print("Taking still images...")
	start = datetime.datetime.now()
	for i in range(50):
		camera.capture("images/stills-1/still-" + str(i) + ".jpg")
	end = datetime.datetime.now()
	duration1 = str(end - start)
	print("Duration at (1024,768): " + duration1)

print("Pausing for 2 seconds...")
sleep(2)

print("Starting up camera at max resolution...")
start = datetime.datetime.now()
with PiCamera() as camera:
	camera.resolution = PiCamera.MAX_RESOLUTION
	print("Max resolution is: " + str(PiCamera.MAX_RESOLUTION))
	end = datetime.datetime.now()
	print("Duration: " + str(end - start))
	
	print("Warming up camera for 2 seconds...")
	camera.start_preview()
	sleep(2)
	print("Camera warmed up!")
	
	print("Taking still images...")
	start = datetime.datetime.now()
	for i in range(50):
		camera.capture("images/stills-2/still-" + str(i) + ".jpg")
	end = datetime.datetime.now()
	duration2 = str(end - start)
	print("Duration at max resolution: " + duration2)

print("Pausing for 2 seconds...")
sleep(2)

print("Starting up camera at (640, 480)...")
start = datetime.datetime.now()
with PiCamera() as camera:
	camera.resolution = (640, 480)
	end = datetime.datetime.now()
	print("Duration: " + str(end - start))
	
	print("Warming up camera for 2 seconds...")
	camera.start_preview()
	sleep(2)
	print("Camera warmed up!")
	
	print("Taking still images...")
	start = datetime.datetime.now()
	for i in range(50):
		camera.capture("images/stills-3/still-" + str(i) + ".jpg")
	end = datetime.datetime.now()
	duration3 = str(end - start)
	print("Duration at (640, 480): " + duration3)

print("SUMMARY")
print("At (1024, 768): " + duration1)
print("At max resolution: " + duration2)
print("At (640, 480): " + duration3)