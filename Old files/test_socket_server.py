import io
import numpy as np
import socket
import struct
import cv2
import datetime
from werkzeug.serving import run_simple
from flask import Flask
import threading

app = Flask(__name__)
   
@app.route("/")
def hello():
    return "Hello World!"

def do_server():
  app.run()

t = threading.Thread(target=do_server)
t.start()

server_socket = socket.socket()
server_socket.bind(('0.0.0.0', 8001))
server_socket.listen(0)

connection = server_socket.accept()[0].makefile('rb')
try:
    while True:
        start = datetime.datetime.now()
        # Read the length of the image as a 32-bit unsigned int. If the
        # length is zero, quit the loop
        image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]
        if not image_len:
            break
        # Construct a stream to hold the image data and read the image
        # data from the connection
        image_stream = io.BytesIO()
        image_stream.write(connection.read(image_len))
        # Rewind the stream, open it as an image with PIL and do some
        # processing on it
        image_stream.seek(0)
        end = datetime.datetime.now()
        print("Received image with length " + str(image_len) + " with duration: " + str(end - start))
        nparr = np.fromstring(image_stream.read(), np.uint8)
finally:
    connection.close()
    server_socket.close()