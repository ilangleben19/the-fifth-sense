import glob, os, sys, time
import cv2 as cv
import numpy as np

if len(sys.argv) < 2:
    print("Fatal Error - Argument Missing: The side of the camera (\"left\" or \"right\") must be passed to slave_pi.py as the 1st command line argument!")
side = sys.argv[1]
if not side in ["right", "left"]:
    print("Fatal Error - Argument Missing: The side of the camera (\"left\" or \"right\") must be passed to slave_pi.py as the 1st command line argument!")

resolution = (1024, 768)
images_dir_name = "./images/calibration/" + str(resolution[0]) + "x" + str(resolution[1])

board_dim = (7, 7)

def generate_undistortion_info(side):
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    objp = np.zeros((7*7,3), np.float32)
    objp[:,:2] = np.mgrid[0:7,0:7].T.reshape(-1,2)

    objpoints, imgpoints = [], []

    images = glob.glob(os.path.join(images_dir_name, side, "*.jpg"))

    for fname in images:
        img = cv.imread(fname)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        #gray = img

        ret, corners = cv.findChessboardCorners(gray, board_dim, None)

        if ret == True:
            print("Ret true: " + str(fname))
            objpoints.append(objp)

            corners2 = cv.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners)

    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
    print(mtx)
    print(dist)
    out = {"mtx": mtx, "dist": dist}
    np.save("undistortion_info_" + side + ".npy", out)

print("Generating undistortion info for " + side + "...")
generate_undistortion_info(side)