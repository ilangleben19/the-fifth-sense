import os, sys, time
from picamera import PiCamera

if len(sys.argv) < 2:
    print("Fatal Error - Argument Missing: The side of the camera (\"left\" or \"right\") must be passed to slave_pi.py as the 1st command line argument!")
side = sys.argv[1]
if not side in ["right", "left"]:
    print("Fatal Error - Argument Missing: The side of the camera (\"left\" or \"right\") must be passed to slave_pi.py as the 1st command line argument!")

resolution = (1024, 768)

images_dir_name = "./images/calibration/" + str(resolution[0]) + "x" + str(resolution[1]) + "/" + side
if not os.path.isdir(images_dir_name):
    os.makedirs(images_dir_name)

with PiCamera() as camera:
    camera.resolution = resolution
    camera.start_preview()
    time.sleep(2)
    for i in range(50):
        input("Press ENTER whenever to take a photo.")
        camera.capture(os.path.join(images_dir_name, str(i) + ".jpg"))