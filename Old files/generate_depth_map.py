#%% Change working directory from the workspace root to the ipynb file location. Turn this addition off with the DataScience.changeDirOnImportExport setting
import os
try:
	os.chdir(os.path.join(os.getcwd(), 'Old files'))
	print(os.getcwd())
except:
	pass

#%%
import cv2 as cv
import glob
from matplotlib import pyplot
import numpy as np
import datetime

id = 2
colour = 0
scale_down_factor = 0
display_scale_down_factor = 2
camera_distortion_info_file = "camera_distortion_info_scale_down_factor_" + str(
    scale_down_factor) + ".npy"

start = datetime.datetime.now()
imgL_orig = cv.imread(str(id) + "myleft.jpg", colour)
imgR_orig = cv.imread(str(id) + "myright.jpg", colour)

#for i in range(scale_down_factor):
for i in range(1):
    imgL_orig = cv.pyrDown(imgL_orig)
    imgR_orig = cv.pyrDown(imgR_orig)
end = datetime.datetime.now()
print("Duration to load images from files and pyrDown() them: " + str(end - start))
    
camera_distortion_info = np.load(camera_distortion_info_file).tolist()
mtx = camera_distortion_info["mtx"]
dist = camera_distortion_info["dist"]

def undistort(img):
    h, w = img.shape[:2]
    newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1,
                                                     (w, h))
    dst = cv.undistort(img, mtx, dist, None, newcameramtx)
    x, y, w, h = roi
    dst = dst[y:y + h, x:x + w]
    return dst

start = datetime.datetime.now()
imgL, imgR = undistort(imgL_orig), undistort(imgR_orig)
#imgL, imgR = imgL_orig, imgR_orig
end = datetime.datetime.now()
print("Duration to undistort images: " + str(end - start))


#%%
window_size = 3
min_disp = 16
num_disp = 16 * 6 - min_disp

start = datetime.datetime.now()
stereo = cv.StereoSGBM_create(
    minDisparity=min_disp,
    numDisparities=num_disp,
    blockSize=16,
    mode=4,
)

disparity = stereo.compute(imgL, imgR).astype(np.float32) / 12.0
#disparity = stereo.compute(imgL, imgR)
end = datetime.datetime.now()
print("Duration to compute stereo depth map: " + str(end - start))

imgL_disp, imgR_disp, disparity_disp = imgL, imgR, disparity
for i in range(display_scale_down_factor):
    imgL_disp = cv.pyrDown(imgL_disp)
    imgR_disp = cv.pyrDown(imgR_disp)
    disparity = cv.pyrDown(disparity)
cv.imshow("left", imgL_disp)
cv.imshow("right", imgR_disp)
cv.imshow("disparity", (disparity - min_disp) / num_disp)
cv.waitKey()
cv.destroyAllWindows()


#%%



#%%



