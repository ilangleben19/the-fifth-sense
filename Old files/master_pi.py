import datetime, flask, io, os, socket, struct, threading
from flask import Flask, request, make_response, Response
import cv2 as cv
import numpy as np
import urllib.parse

can_proceed = False
images_dir_name = ""
while not can_proceed:
  now = datetime.datetime.now()
  images_dir_name = ("./images/raw/started " + now.isoformat()).replace(":", "-")
  if not os.path.isdir(images_dir_name):
    can_proceed = True
    os.makedirs(images_dir_name)

server_port = 8081
data_ports = {"left": 8082, "right": 8083}

app = Flask(__name__)

left_ready = False
right_ready = False

latest_left = None
latest_left_buffer = None
latest_right = None
lastest_right_buffer = None

@app.route("/monitor")
def monitor():
  global latest_left, latest_right
  return "<image src='/current_left' />"

@app.route("/current_left")
def show_current_left():
  global latest_left_buffer, latest_left
  return flask.Response(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n'
    + cv.imencode(".jpg", latest_left)[1].tobytes() + b'\r\n\r\n', mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route("/current_right")
def show_current_right():
  pass

@app.route("/connectivity_test")
def connectivity_test():
  return "Ready!"

@app.route("/register_left_ready")
def register_left_ready():
  global left_ready
  left_ready = True
  print("LEFT: Left pi is ready!")
  return "Ok!"

@app.route("/register_right_ready")
def register_right_ready():
  global right_ready
  right_ready = True
  print("RIGHT: Right pi is ready!")
  return "Ok!"

@app.route("/get_starting_information")
def send_starting_info():
  global left_ready, right_ready, starting_time
  if not (left_ready and right_ready):
    print("Starting info requested; left_ready=" + str(left_ready) + " and right_ready=" + str(right_ready))
    return "Not ready!"
  print("Setting up starting info...")
  if not "starting_time" in globals():
    starting_time = datetime.datetime.now() + datetime.timedelta(seconds=5)
  print("Both pi's are ready. Now is: " + str(datetime.datetime.now())
    + " | Will start to send data at: " + str(starting_time)
    + " | In: " + str(starting_time - datetime.datetime.now()))
  return str(starting_time)

@app.route("/")
def home_page():
  return "Nothing to see here :)"

@app.route("/shutdown")
def shutdown():
  shutdown_server()
  return "Server is shutting down..."

def shutdown_server():
  shutdown_func = request.environ.get("werkzeug.server.shutdown")
  if shutdown_func is None:
    raise RuntimeError(
      "Runtime Error: shutdown_server() was called, but Flask server is not running!")
  print("Received request to shutdown server, shutting down...")
  shutdown_func()

def run_server():
  app.run(host="0.0.0.0", port=server_port)

def left_listener():
  global latest_left
  while True:
    left_server_socket = socket.socket()
    left_server_socket.bind(('0.0.0.0', data_ports["left"]))
    left_server_socket.listen(0)

    left_connection = left_server_socket.accept()[0].makefile("rb")
    try:
      while True:
        left_start = datetime.datetime.now()
        left_image_len = struct.unpack("<L", left_connection.read(struct.calcsize("<L")))[0]
        if not left_image_len:
          break
        left_image_stream = io.BytesIO()
        left_image_stream.write(left_connection.read(left_image_len))
        left_image_stream.seek(0)
        left_end = datetime.datetime.now()
        left_nparr = np.fromstring(left_image_stream.read(), np.uint8)
        print("LEFT: Received image with length " + str(left_image_len)
          + " with duration " + str(left_end - left_start))
        left_img = cv.imdecode(left_nparr, cv.IMREAD_UNCHANGED)
        latest_left = left_img
        #cv.imwrite(os.path.join(images_dir_name, "left " + left_end.isoformat().replace(":", "-") + ".jpg"), left_img)
        
    except:
      print("LEFT: An error occurred. Most likely, a client disconnected.")
      left_connection.close()
      left_server_socket.close()
    finally:
      left_connection.close()
      left_server_socket.close()

def right_listener():
  global latest_right
  while True:
    right_server_socket = socket.socket()
    right_server_socket.bind(('0.0.0.0', data_ports["right"]))
    right_server_socket.listen(0)

    right_connection = right_server_socket.accept()[0].makefile("rb")
    try:
      while True:
        right_start = datetime.datetime.now()
        right_image_len = struct.unpack("<L", right_connection.read(struct.calcsize("<L")))[0]
        if not right_image_len:
          break
        right_image_stream = io.BytesIO()
        right_image_stream.write(right_connection.read(right_image_len))
        right_image_stream.seek(0)
        right_end = datetime.datetime.now()
        right_nparr = np.fromstring(right_image_stream.read(), np.uint8)
        print("RIGHT: Received image with length " + str(right_image_len)
          + " with duration " + str(right_end - right_start))
        right_img = cv.imdecode(right_nparr, cv.IMREAD_UNCHANGED)
        latest_right = right_img
        #cv.imwrite(os.path.join(images_dir_name, "right " + right_end.isoformat().replace(":", "-") + ".jpg"), right_img)
    except:
      print("right: An error occurred. Most likely, a client disconnected.")
      right_connection.close()
      right_server_socket.close()
    finally:
      right_connection.close()
      right_server_socket.close()


threads = {}
def make_thread(name, func, start=False):
  threads[name] = threading.Thread(target=func)
  if start:
    threads[name].start()

make_thread(name="server_thread", func=run_server, start=True)
make_thread(name="left_receiver_thread", func=left_listener, start=True)
make_thread(name="right_receiver_thread", func=right_listener, start=True)