import datetime, io, picamera, requests, socket, struct, sys, time
import urllib.parse
from picamera import PiCamera

if len(sys.argv) < 2:
    print("Fatal Error - Argument Missing: The side of the camera (\"left\" or \"right\") must be passed to slave_pi.py as the 1st command line argument!")
side = sys.argv[1]
if not side in ["right", "left"]:
    print("Fatal Error - Argument Missing: The side of the camera (\"left\" or \"right\") must be passed to slave_pi.py as the 1st command line argument!")

server_port = 8081
server_address = "http://10.42.0.1:" + str(server_port)
data_ports = {"left": 8082, "right": 8083}

connectivity_test_url = urllib.parse.urljoin(server_address, "connectivity_test")
register_side_ready_url = urllib.parse.urljoin(server_address, "register_" + side.strip() + "_ready")
starting_info_url = urllib.parse.urljoin(server_address, "get_starting_information")

server_connectivity_success_message = "Ready!"
server_received_side_ready_message = "Ok!"
server_not_ready_message = "Not ready!"

server_connectivity_success = False
server_received_side_ready = False
server_ready = False
should_loop = True

image_send_time_interval = datetime.timedelta(seconds=1)
resolution = (1024, 768)

while not server_connectivity_success:
    try:
        page = requests.get(connectivity_test_url)
        status_code = int(page.status_code)
        if 200 <= status_code < 400:
            if page.text.strip().lower() == server_connectivity_success_message.strip().lower():
                server_connectivity_success = True
                continue
    except requests.exceptions.ConnectionError:
        pass
    time.sleep(500)

while not server_received_side_ready:
    page = requests.get(register_side_ready_url)
    status_code = int(page.status_code)
    if 200 <= status_code < 400:
        if page.text.strip().lower() == server_received_side_ready_message.strip().lower():
            server_received_side_ready = True
            continue

while not server_ready:
    page = requests.get(starting_info_url)
    status_code = int(page.status_code)
    if 200 <= status_code < 400:
        if page.text.strip().lower() != server_not_ready_message.strip().lower():
            server_ready = True
            print("MASTER PI SENT: " + page.text)

while should_loop:
    client_socket = socket.socket()
    client_socket.connect(('10.42.0.1', data_ports[side]))

    connection = client_socket.makefile('wb')
    try:
        camera = picamera.PiCamera()
        camera.resolution = resolution
        camera.start_preview()
        time.sleep(2)

        start = time.time()
        stream = io.BytesIO()
        for foo in camera.capture_continuous(stream, 'jpeg'):
            print(side.strip().upper() + ": Beginning to send image at " + str(datetime.datetime.now()))
            connection.write(struct.pack('<L', stream.tell()))
            connection.flush()
            stream.seek(0)
            connection.write(stream.read())
            stream.seek(0)
            stream.truncate()

            now = datetime.datetime.now()
            next_unrounded = now + image_send_time_interval
            next_rounded = next_unrounded + datetime.timedelta(microseconds=-now.microsecond)
            while datetime.datetime.now() < next_rounded:
                pass
        connection.write(struct.pack('<L', 0))
    finally:
        connection.close()
        client_socket.close()
