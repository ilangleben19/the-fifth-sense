import time
from openal import *

try:
    get_sound = lambda: Buffer(WaveFile("sounds/song.wav"))

    zpos = 2.5
    volume_multiplier = 15
    points = []
    for x in range(8):
        for y in range(6):
            volume = 0
            if (x, y) in [(0, 0), (0, 1), (0, 2)]:
                volume = 1.5
            if (x, y) in [(1, 0), (1, 1)]:
                volume = 3.5
            if x > 4 or (x, y) in [(4, 0), (4, 1)]:
                volume = 2.25
            points.append((x - 3.5, y - 2.5, volume, 2))

    source = Source(get_sound())
    source.play()
    time.sleep(0.18640776699 * 4)
    while True:
        for point in points:
            if point[2] == 0: pass
            source.set_position((point[0], point[1], zpos))
            source.set_gain((point[2] + 0.09) * volume_multiplier)
            time.sleep(0.18640776699 / 2)

    oalQuit()
except KeyboardInterrupt:
    oalQuit()
    raise