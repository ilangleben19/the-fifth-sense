import cv2
import numpy as np
from matplotlib import pyplot as plt

def undistort(original):
    camera_distortion_info = np.load("undistortion_info.npy").tolist()
    mtx, dist = camera_distortion_info["mtx"], camera_distortion_info["dist"]
    h, w = original.shape[:2]
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))
    undistorted = cv2.undistort(original, mtx, dist, None, newcameramtx)
    x, y, w, h = roi
    return undistorted[y:y+h, x:x+w]

window_size = 3
min_disp = 16
num_disp = 16*16+112-min_disp
stereo = cv2.StereoSGBM_create(minDisparity = min_disp,
    numDisparities = num_disp,
    blockSize = 8,
    P1 = 8*3*window_size**2,
    P2 = 32*3*window_size**2,
    disp12MaxDiff = 1,
    uniquenessRatio = 10,
    speckleWindowSize = 100,
    speckleRange = 32
)

id = 2
left = cv2.imread("./left" + str(id) + ".jpg", 0)
right = cv2.imread("./right" + str(id) + ".jpg", 0)

scale_down_count = 0
for i in range(scale_down_count):
    left = cv2.pyrDown(left)
    right = cv2.pyrDown(right)

left = undistort(left)
right = undistort(right)

disparity = stereo.compute(left, right).astype(np.float32) / 16.0
plt.imshow(disparity, "gray")
plt.show()