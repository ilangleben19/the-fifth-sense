import time
from openal import *

try:
    get_sound = lambda: Buffer(WaveFile("sounds/c2.wav"))

    zpos = 3
    volume_multiplier = 1
    points = []
    for x in range(8):
        for y in range(6):
            volume = 0
            if (x, y) in [(0, 0), (0, 1), (0, 2)]:
                volume = 1.5
            if (x, y) in [(1, 0), (1, 1)]:
                volume = 3.5
            if x > 4 or (x, y) in [(4, 0), (4, 1)]:
                volume = 2
            points.append((x - 3.5, y - 2.5, volume))

    sources = []
    for point in points:
        source = Source(get_sound())
        source.set_position((point[0], point[1], zpos))
        source.set_gain(point[2] * volume_multiplier)
        source.set_pitch((point[1] + 2.5) / 8 * 10)
        sources.append(source)

    while True:
        for source in sources:
            if source.gain == 0: pass
            source.play()
            time.sleep(0.1)
        time.sleep(0.3)

    oalQuit()
except KeyboardInterrupt:
    oalQuit()
    raise