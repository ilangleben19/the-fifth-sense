#region master.py
with open("master.py", "r") as file:
    old_contents = [line.replace("\n", "") for line in file.readlines()]

without_regions = list(filter(lambda line: not ("#region" in line or "#endregion" in line), old_contents))
without_check_interrupt_calls = list(filter(lambda line: "check_interrupt()" not in line.strip(), without_regions))
without_check_interrupt_params = [line.replace("(check_interrupt)", "()") for line in [
    line.replace(", check_interrupt)", ")") for line in without_check_interrupt_calls
]]
without_all_check_interrupts = [line for line in without_check_interrupt_params
    if "with util.InterruptableRegion() as interrupt_manager:" not in line
        and "check_interrupt = util.get_interruption_checker(interrupt_manager, on_interrupt)" not in line
        and "def on_interrupt(): pass" not in line]

new_contents = "\n".join([line.replace("import config, util", "import config") for line in without_all_check_interrupts])
with open("print/master.py", "w") as file:
    file.write(new_contents.replace("\n\n\n", "\n\n"))

print("In print/master.py:")
print("\t - Remove the Flask endpoint to restart server as it's only called by slave check_interrupt")
print()
#endregion master.py

#region slave.py
with open("slave.py", "r") as file:
    old_contents = [line.replace("\n", "") for line in file.readlines()]

without_check_interrupt_calls = list(filter(lambda line: "check_interrupt()" not in line.strip(), old_contents))
without_all_check_interrupts = [line for line in without_check_interrupt_calls
    if "def on_interrupt(): get_page(config.server_restart_url)" not in line
        and "with util.InterruptableRegion() as interrupt_manager:" not in line
        and "check_interrupt = util.get_interruption_checker(interrupt_manager, on_interrupt)" not in line
]

new_contents = "\n".join([line.replace("import config, util", "import config") for line in without_all_check_interrupts])
with open("print/slave.py", "w") as file:
    file.write(new_contents.replace("\n\n\n", "\n\n"))
#endregion slave.py

#region config.py
with open("config.py", "r") as file:
    old_contents = [line.replace("\n", "") for line in file.readlines()]

new_contents = "\n".join([line for line in old_contents])
with open("print/config.py", "w") as file:
    file.write(new_contents.replace("\n\n\n", "\n\n"))
#endregion config.py

#region generate_undistortion_info.py
with open("generate_undistortion_info.py", "r") as file:
    with open("print/generate_undistortion_info.py", "w") as out_file:
        out_file.write(file.read())
#endregion generate_undistortion_info.py

#region capture_calibration_images.py
with open("capture_calibration_images.py", "r") as file:
    with open("print/capture_calibration_images.py", "w") as out_file:
        out_file.write(file.read())
#endregion capture_calibration_images.py