import datetime, os, sys, time
from picamera import PiCamera

import config

try:
    count = int(sys.argv[1])
except:
    count = 20

start_time = datetime.datetime.now()
images_dir = "/home/pi/calibration_images/" + str(start_time) + "/"
if not os.path.isdir(images_dir):
    os.makedirs(images_dir)

with PiCamera() as camera:
    camera.resolution = config.image_resolution
    camera.start_preview()
    time.sleep(2)
    for i in range(count):
        input("Press ENTER to take a photo.")
        camera.capture(os.path.join(images_dir, str(i) + ".jpg"))