import pytz
from datetime import datetime
from flask import Flask
from flask_socketio import SocketIO, emit

app = Flask(__name__)
socketio = SocketIO(app)

@app.route("/api")
def api_home():
    return "This works!"

id = 0

@socketio.on("send")
def send_dummy_snapshot():
    global id
    id = id + 1
    emit("snapshot", {
        "id": id,
        "timeTaken": datetime.now(pytz.timezone("Canada/Pacific")).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],
        "leftUrl": "https://www.thesprucepets.com/thmb/IQGCoLl7dB6RaigKHDZbw3myQvg=/450x0/filters:no_upscale():max_bytes(150000):strip_icc()/portrait-if-a-spitz-pomeranian_t20_v3o29E-5ae9bbdca18d9e0037d95983.jpg",
        "leftCaption": str(id),
        "rightUrl": "https://www.thesprucepets.com/thmb/IQGCoLl7dB6RaigKHDZbw3myQvg=/450x0/filters:no_upscale():max_bytes(150000):strip_icc()/portrait-if-a-spitz-pomeranian_t20_v3o29E-5ae9bbdca18d9e0037d95983.jpg",
        "rightCaption": "dog"
    })
    return

@socketio.on("connect")
def on_connect():
    print("Connected!")
    send_dummy_snapshot()
    return


if __name__ == '__main__':
    socketio.run(app, port=666)