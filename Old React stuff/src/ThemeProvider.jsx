import React from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { blue, gray } from "@material-ui/core/colors";

export default (props) => (
  <MuiThemeProvider theme={createMuiTheme({
    palette: {
      primary: blue,
      secondary: gray,
    },
    typography: {
      fontFamily: [
        "-apple-system",
        "BlinkMacSystemFont",
        "Roboto",
        "Product Sans",
        "Segoe UI",
        "Calibri",
        "Arial",
        "sans-serif"
      ].join(','),
      useNextVariants: true,
    },
    ...props.themeData
  })}>
    {props.children}
  </MuiThemeProvider>
);