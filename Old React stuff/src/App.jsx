import React from 'react';
import {
  CssBaseline,
  withStyles,
} from "@material-ui/core";

import "./fonts.css";
import './index.css';

import "typeface-roboto";

import SnapshotView from "./components/snapshot/SnapshotView";
import ThemeProvider from "./ThemeProvider";
import TopBar from "./components/topBar/TopBar";

const styles = {
  root: {
    display: "flex",
  },
};

export default withStyles(styles) ((props) => (
  <ThemeProvider>
    <CssBaseline />
    <div className={props.classes.root}>
      <TopBar />
      <SnapshotView />
    </div>
  </ThemeProvider>
));