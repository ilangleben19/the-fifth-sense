import React from 'react';

import {
    Divider,
    Drawer,
    IconButton,
    List,
    ListItem,
    ListItemText,
    withStyles,
} from "@material-ui/core";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

const styles = (theme) => ({
    drawerHeader: {
        display: "flex",
        alignItems: "center",
        padding: "0 8px",
        ...theme.mixins.toolbar,
        justifyContent: "flex-end",
    },    
    list: {
        width: 250,
    },
});

export default withStyles(styles) ((props) => (
    <Drawer anchor="left"
            open={props.open}
            onClose={props.onClose}>
            <div className={props.classes.drawerHeader}>
                <IconButton color="primary" onClick={props.onClose}>
                    <ChevronLeftIcon />
                </IconButton>
            </div>
            <Divider />
            <div
                tabIndex={0}
                role="button"
                onClick={props.onClose}
                onKeyDown={props.onClose}>
                <div className={props.classes.list}>
                    <List>
                        {["Nothing to see here yet!"].map((text, index) => (
                            <ListItem button key={text}>
                                <ListItemText primary={text} />
                            </ListItem>
                        ))}
                    </List>
                </div>
            </div>
    </Drawer>
));