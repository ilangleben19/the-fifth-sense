import React, { Component } from 'react';
import classNames from "classnames";

import {
    AppBar,
    IconButton,
    Toolbar,
    Typography,
    withStyles,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";

import TopBarDrawer from "./TopBarDrawer";

const styles = (theme) => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
});

export default withStyles(styles) (class TopBar extends Component {
    constructor() {
        super();
        this.state = {drawerOpen: false};
    }

    render() {
        return (
            <AppBar
                title="Vis-ability Control Center"
                position="fixed"
                className={classNames("top-bar-appbar", this.props.classes.appBar)}
                {...this.props.appBarProps}>
                <Toolbar>
                    <IconButton
                        className={this.props.classes.menuButton}
                        color="inherit"
                        onClick={() => this.setState({drawerOpen: true})}
                        aria-label="Menu">
                        <MenuIcon />
                    </IconButton>

                    <TopBarDrawer
                        open={this.state.drawerOpen}
                        onClose={() => this.setState({drawerOpen: false})} />

                    <Typography
                        className={classNames("top-bar-typography", this.props.classes.typography)}
                        variant="h6"
                        color="inherit"
                        {...this.props.typographyProps}>
                        Vis-ability Control Center
                    </Typography>
                </Toolbar>
            </AppBar>
        );
    }
});