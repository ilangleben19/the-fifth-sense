//
// Borrowed from https://github.com/dennybritz/neal-react/blob/master/js/components/figure.jsx
// Altered slightly to
// (a) use <Img> from react-image;
// (b) remove static propTypes;
// (c) be a function component;
// (d) have a default circular progress spinner to indicate loading
//

import React from "react";
import {
  CircularProgress,
  withStyles,
} from "@material-ui/core";

import classNames from "classnames";
import Img from "react-image";

const styles = (theme) => ({
  figure: {
    margin: theme.spacing.unit * 2,
  },
  circularProgress: {
    marginBottom: theme.spacing.unit * 3,
    marginTop: theme.spacing.unit * 1,
  },
});

export default withStyles(styles) ((props) => {
  const loader = props.loader
    ? props.loader
    : <CircularProgress className={props.classes.circularProgress} color="primary" /> ;
  return (
    <figure {... props} className={classNames("figure", props.classes.figure, props.className)}>
      <Img
        src={props.src}
        alt={props.caption}
        title="test12345"
        loader={loader}
        unloader={loader} />
      <figcaption className="figure-caption">{props.caption}</figcaption>
    </figure>
  );
});