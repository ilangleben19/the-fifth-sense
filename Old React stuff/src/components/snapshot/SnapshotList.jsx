import React from "react";
import classNames from "classnames";

import {
    Button,
    List,
    withStyles,
} from "@material-ui/core";

import SnapshotListItem from "./SnapshotListItem";

const styles = (theme) => ({
    root: {
        flexGrow: 1,
    },
    list: {
        borderLeft: "1px solid " + theme.palette.divider,
        borderRight: "1px solid " + theme.palette.divider,
        border: "1px solid " + theme.palette.divider,
        overflowY: "auto",
    },
    button: {
        position: "absolute",
        top: theme.spacing.unit * 3,
        left: theme.spacing.unit * 3,
        zIndex: 2000,
    },
});

export default withStyles(styles) ((props) => (
    <div className={classNames(props.classes.root, props.containerClassName)}>
        <div><List className={props.classes.list}>
            {Array.from(props.snapshots).map((snapshot, index, array) => (
                <SnapshotListItem
                    key={snapshot.id}
                    id={snapshot.id}
                    timeTaken={snapshot.timeTaken}
                    selected={snapshot.id === props.selectedSnapshotId}
                    starred={props.starredSnapshotIds.includes(snapshot.id)}
                    onStarButtonClick={() => props.onStarButtonClick(snapshot.id)}
                    onClickHandler={() => props.onListItemClick(index)}
                    lastItem={index === array.length - 1} />
            ))}
        </List></div>
        <Button className={props.classes.button} variant="fab" color="primary">Test</Button>
    </div>
));