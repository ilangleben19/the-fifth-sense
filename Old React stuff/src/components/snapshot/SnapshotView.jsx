import React, { Component } from 'react';

import {
    Drawer,
    withStyles,
} from "@material-ui/core";

import socketIOClient from "socket.io-client";

import SnapshotDisplay from "./SnapshotDisplay";
import SnapshotList from "./SnapshotList";

const drawerWidth = 400;
const styles = (theme) => ({
    root: {
        display: "flex",
        flexGrow: 1,
    },
    toolbar: theme.mixins.toolbar,
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      flexGrow: 1,
      height: "100%",
    },
    list: {
        flexGrow: 1,
        height: "100%",
    },
    drawerPaper: {
      width: drawerWidth,
    },
    mainDisplayContainer: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
    },
});

export default withStyles(styles) (class SnapshotView extends Component {
    constructor() {
        super();
        this.state = {
            snapshots: [],
            starredSnapshotIds: [],
            selectedSnapshotIndex: -1,
            response: false,
            endpoint: "localhost:666",
        };
    }
    componentDidMount() {
        const socket = socketIOClient(this.state.endpoint);
        socket.on("snapshot", (snapshot) => {
            snapshot.timeTaken = new Date(snapshot.timeTaken);
            var tempSnapshots = this.state.snapshots;            
            tempSnapshots.unshift(snapshot);
            var tempSelectedIndex = this.state.selectedSnapshotIndex;
            if (tempSelectedIndex >= 0)
                tempSelectedIndex++;
            else
                tempSelectedIndex = 0;
            this.setState({snapshots: tempSnapshots, selectedSnapshotIndex: tempSelectedIndex});
        });
        setInterval(() => socket.emit("send"), 1000);
    }
    onStarButtonClick = (snapshotId) => {
        var tempStarredSnapshotIds = this.state.starredSnapshotIds;
        if (tempStarredSnapshotIds.includes(snapshotId))
            tempStarredSnapshotIds = tempStarredSnapshotIds.filter((value) => value !== snapshotId);
        else
            tempStarredSnapshotIds.push(snapshotId);
        this.setState({starredSnapshotIds: tempStarredSnapshotIds});
    }
    render() {
        return (
            <div className={this.props.classes.root}>
                <Drawer className={this.props.classes.drawer}
                    variant="permanent"
                    classes={{paper: this.props.classes.drawerPaper}}>
                    <div className={this.props.classes.toolbar} />
                        <SnapshotList
                            snapshots={this.state.snapshots}
                            starredSnapshotIds={this.state.starredSnapshotIds}
                            selectedSnapshotId={
                                this.state.selectedSnapshotIndex >= 0
                                ? this.state.snapshots[this.state.selectedSnapshotIndex].id
                                : -1
                            }
                            onStarButtonClick={this.onStarButtonClick}
                            onListItemClick={(clickedSnapshotIndex) => this.setState({selectedSnapshotIndex: clickedSnapshotIndex})} />
                </Drawer>
                
                <main className={this.props.classes.mainDisplayContainer}>
                    <div className={this.props.classes.toolbar} />
                    {this.state.selectedSnapshotIndex >= 0
                        ? <SnapshotDisplay
                            snapshot={this.state.snapshots[this.state.selectedSnapshotIndex]} />
                        : <div />}
                </main>
            </div>
        );
    }
});