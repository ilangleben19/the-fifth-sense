import React, { Component } from 'react';

import {
    Grid,
    Paper,
    Typography,
    withStyles,
} from "@material-ui/core";

import Figure from "../Figure";

const styles = (theme) => ({
    root: {
        flexGrow: 1,
        marginLeft: theme.spacing.unit * 2,
        marginRight: theme.spacing.unit * 2,
    },
    paper: {
        padding: theme.spacing.unit * 2,
    },
});

export default withStyles(styles) (class SnapshotDisplay extends Component {
    render() {
        const { classes } = this.props;
        const { id, timeTaken, leftUrl, leftCaption, rightUrl, rightCaption } = this.props.snapshot;
        //rightUrl = "https://upload.wikimedia.org/wikipedia/commons/3/3f/Fronalpstock_big.jpg";

        return (
            <div className={classes.root}>
                <Typography variant="h4" align="left">
                    Snapshot #{id}
                </Typography>
                <Typography variant="h5" gutterBottom align="left">
                    Taken on {
                        timeTaken.toLocaleDateString("en-US", {
                            weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric',
                        }) + ", " + timeTaken.getMilliseconds() + " ms"
                    }
                </Typography>
                <Grid container
                    spacing={40}
                    className={classes.grid}
                    alignItems="flex-start"
                    justify="space-evenly">
                    {[
                        [leftUrl, leftCaption, "left"],
                        [rightUrl, rightCaption, "right"]
                    ].map(([url, caption, directionString]) => (
                        <Grid item xs={12} lg={6} className={classes.gridCell} key={directionString}>
                            <Paper className={classes.paper}>
                                <Figure src={url} caption={caption ? caption : url} />
                            </Paper>
                        </Grid>
                    ))}
                </Grid>
            </div>
        );
    }
});