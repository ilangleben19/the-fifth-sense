import React from "react";

import {
    IconButton,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    withStyles,
} from "@material-ui/core";
import {
    CameraAlt,
    Star,
    StarBorder,
} from "@material-ui/icons";

const styles = {

};

export default withStyles(styles) ((props) => (
    <ListItem key={props.id} button
        onClick={props.onClickHandler}
        selected={props.selected}
        divider={!props.lastItem}>
        <CameraAlt />
        <ListItemText
            primary={"Snapshot #" + props.id}
            secondary={props.timeTaken.toLocaleDateString("en-US", {
                year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric',
            }) + ", " + props.timeTaken.getMilliseconds() + " ms"} />
        <ListItemSecondaryAction>
            <IconButton
                aria-label="favourite"
                color="secondary"
                onClick={props.onStarButtonClick}>
                {props.starred ? <Star /> : <StarBorder />}
            </IconButton>
        </ListItemSecondaryAction>
    </ListItem>
));