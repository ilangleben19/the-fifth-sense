###
# This script runs on each Pi Zero W.
# We denote the Pi Zero W's as slaves and the 3B+ as master.
###

import datetime, io, os, requests, socket, struct, sys, time
from picamera import PiCamera

import config, util

# Simple utility function to try to connect to a website and return the response
def get_page(url):
    try:
        page = requests.get(url)
        if 200 <= int(page.status_code) < 400:
            return page.text.strip()
        else:
            return False
    except requests.exceptions.ConnectionError:
        return False

script_log_dir = "/home/pi/script_log_dir/"
if not os.path.exists(script_log_dir):
    os.makedirs(script_log_dir)

start_time = datetime.datetime.now()
def log(message):
    with open(script_log_dir + str(start_time) + ".log", "a+") as log_file:
        log_output = "[" + str(datetime.datetime.now()) + "] " + str(message)
        log_file.write(log_output + "\n")
        print(log_output)

def on_interrupt(): pass

def run_slave(side):
    with util.InterruptableRegion() as interrupt_manager:
        while True:
            side in config.sides or sys.exit("Fatal error: side provided ('" + side + ") is not '" + config.side_left + "' or '" + config.side_right + "'")
            
            log("Slave script starting up with side='" + side + "'")
            check_interrupt = util.get_interruption_checker(interrupt_manager, on_interrupt)
            connected, confirmed_side_ready, server_ready_to_start, should_loop = False, False, False, False

            # Step 1: Wait until the server can be connected to
            while not connected:
                connected = (get_page(config.connectivity_test_url) == config.server_connectivity_success_message.strip())
                check_interrupt()
                time.sleep(0.300)
            log("Confirmed connection to server!")
            check_interrupt()

            # Step 2: Confirm to the server that this side is ready
            while not confirmed_side_ready:
                confirmed_side_ready = (get_page(config.get_register_side_ready_url(side)) == config.get_server_received_side_ready_message(side).strip())
                check_interrupt()
            log("Confirmed " + side + " side ready!")
            check_interrupt()

            # Step 3: Confirm that the server is ready and get starting time
            while not server_ready_to_start:
                starting_time = get_page(config.starting_info_url)
                server_ready_to_start = (str(starting_time).strip() != config.server_not_ready_message.strip())
                server_ready_to_start = server_ready_to_start and str(starting_time).strip() != "False"
                check_interrupt()
            log("Received starting time: " + str(starting_time))
            starting_time = datetime.datetime.strptime(starting_time, "%Y-%m-%d %H:%M:%S.%f")
            check_interrupt()

            # Step 4: Wait until starting time
            while datetime.datetime.now() < starting_time:
                check_interrupt()
                pass
            check_interrupt()

            # Step 5: Continously transmit image data
            should_loop = True
            while should_loop:
                client_socket = socket.socket()
                client_socket.connect((config.server_ip_address_for_slaves, config.server_image_transfer_ports[side]))

                connection = client_socket.makefile("wb")
                try:   
                    camera = PiCamera()
                    camera.resolution = config.image_resolution
                    camera.start_preview()
                    time.sleep(2)

                    start = time.time()
                    stream = io.BytesIO()
                    for foo in camera.capture_continuous(stream, "jpeg"):
                        connection.write(struct.pack("<L", stream.tell()))
                        connection.flush()
                        stream.seek(0)
                        connection.write(stream.read())
                        stream.seek(0)
                        stream.truncate()

                        now = datetime.datetime.now()
                        log("Side " + side.strip() + " sent image at " + str(now))
                        next_unrounded = now + config.image_send_time_interval
                        next_rounded = next_unrounded + datetime.timedelta(microseconds=-now.microsecond) # Round to nearest second
                        while datetime.datetime.now() < next_rounded:
                            check_interrupt()
                            pass
                    connection.write(struct.pack("<L", 0))
                finally:
                    connection.close()
                    client_socket.close()
                    should_loop = False
            check_interrupt()

            # Step 6: Restart until next time
            log("Script finished; restarting...")
    
if __name__ == "__main__":
    run_slave(input("Enter which side to run as: ").strip())