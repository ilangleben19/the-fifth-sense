#!/usr/bin/python3

import datetime, os, sh, subprocess, sys, time
from urllib.request import urlopen
from sh import git

try:
    import httplib
except:
    import http.client as httplib

def have_internet():
    conn = httplib.HTTPConnection("www.google.com", timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except:
        conn.close()
        return False

update_interval = 15 # Seconds
daemon_log_dir = "/home/pi/daemon_logs/"
working_dir = "/home/pi/vis-ability/" if os.path.exists("/home/pi/vis-ability/") else ("/home/pi/Vis-ability/" if os.path.exists("/home/pi/Vis-ability/") else "/home/pi/the-fifth-sense/")

def git_call(arguments):
    try:
        ret = git("--git-dir=" + working_dir + ".git/", "--work-tree=" + working_dir, *arguments)
        return ret
    except:
        return "There was an error!!!"

start_script = lambda: subprocess.Popen(["python3", "/home/pi/script.py"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

if not os.path.exists(daemon_log_dir):
    os.makedirs(daemon_log_dir)

start_time = datetime.datetime.now()
def log(message):
    with open(daemon_log_dir + str(start_time) + ".log", "a+") as log_file:
        log_output = "[" + str(datetime.datetime.now()) + "] " + str(message)
        log_file.write(log_output + "\n")
        print(log_output)

log("Starting up...")
log("Running ./script.py")
start_script()

while True:
    if not have_internet():
        continue

    log("Checking status for git repo in \"" + working_dir + "\"...")
    if git_call(["fetch"]) == "There was an error!!!":
        continue
    status = git_call(["status"])

    if "up-to-date" in status or "up to date" in status:
        log("Code is up to date.")
    else:
        log("Update available. Killing script.py...")
        for script in ["script.py", "master.py", "left.py", "right.py", "slave.py"]:
            subprocess.call(["pkill", "-f", script])

        log("Pulling git repo in \"" + working_dir + "\"...")
        git_call(["pull"])

        log("Restarting ./script.py...")
        start_script()
    
    log("Sleeping for " + str(update_interval) + " seconds")
    time.sleep(update_interval)