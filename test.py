from flask import Flask, request

server = Flask(__name__)

@server.route("/test")
def test():
    print(request.args.get("x"))
    return "kk"

server.run(host="0.0.0.0")