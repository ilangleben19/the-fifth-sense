from openal import *
import time
import random

source1 = Source(Buffer(WaveFile("sounds/c2.wav")))
xpos = 2.0
ypos = 2.0
zpos = -2.0
source1.set_position((xpos, ypos, 1.0))
#source1.set_position((random.uniform(-2.5, 2.5), random.uniform(-2.5, 2.5), random.uniform(-2.5, 2.5)))

#source2 = Source(Buffer(WaveFile("sounds/bounce.wav")))
#source2.set_position((random.uniform(-2.5, 2.5), random.uniform(-2.5, 2.5), random.uniform(-2.5, 2.5)))

listener = oalGetListener()
listener.set_gain(3.0)

while True:
    source1.play()
    xpos -= 0.10
    ypos -= 0.10
    zpos += 0.20
    source1.set_position((xpos, ypos, 1.0))
    #source2.play()

    timer = 0
    #while source1.get_state() == AL_PLAYING or source2.get_state() == AL_PLAYING:
    while source1.get_state() == AL_PLAYING:
        if timer > 100: break
        time.sleep(0.001)
        timer += 1

oalQuit()