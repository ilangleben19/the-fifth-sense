import cv2, datetime, io, openal, os, socket, struct, sys, threading, time
from flask import Flask, request
import numpy as np

import config, util

#region global variables
# Using lock for global variables because the program is multi-threaded
_lock = threading.Lock()
with _lock:
    # Define some global variables
    _script_start_time = datetime.datetime.now()

    _script_dir = "/home/pi/the-fifth-sense/"
    _script_log_folder = "/home/pi/Drive/script_logs/"
    _images_folder, _depth_maps_folder, _higher_level_maps_folder = "", "", ""

    _threads = {}

    _left_ready, _right_ready = False, False
    _image_transmission_starting_time = None

    _received_latest_left, _received_latest_right = False, False
    _latest_left, _latest_right = None, None

    _mtx, _dist, _newcameramtx, _roi = None, None, None, None

    _depth_map_ready, _depth_map = False, None

    _audio_source_data, _audio_source = [], None
#endregion global variables

#region logging
def log(message):
    with open(_script_log_folder + str(_script_start_time) + ".txt", "a+") as log_file:
        log_output = "[" + str(datetime.datetime.now()) + "] " + str(message)
        log_file.write(log_output + "\n")
        print(log_output)
#endregion logging

#region threading utilities
def on_interrupt(): pass

# Simple utility function to create and (possibly) start a thread with a runner function
def make_thread(name, func, start=False):
    global _threads
    log("Creating thread \"" + name + "\"" + (" and starting it" if start else "") + "...")
    _threads[name] = threading.Thread(target=func)
    if start:
        _threads[name].start()
#endregion threading utilities

#region folders
# Basic logic to set up the folders to store logs and data
# In development, all folders were stored in the Google Drive folder
#   to make monitoring the device's activity easier
def set_up_log_folder():
    global _script_log_folder
    if not os.path.exists(_script_log_folder):
        os.makedirs(_script_log_folder)

def set_up_images_folder():
    global _images_folder
    log("Setting up images folder")
    _images_folder = "/home/pi/Drive/images/" + str(_script_start_time) + "/"
    if not os.path.isdir(_images_folder):
        os.makedirs(_images_folder)

def set_up_depth_maps_folder():
    global _depth_maps_folder
    log("Setting up depth maps folder")
    _depth_maps_folder = "/home/pi/Drive/depth_maps/" + str(_script_start_time) + "/"
    if not os.path.isdir(_depth_maps_folder):
        os.makedirs(_depth_maps_folder)

def set_up_higher_level_maps_folder():
    global _higher_level_maps_folder
    log("Setting up higher level maps folder")
    _higher_level_maps_folder = "/home/pi/Drive/higher_level_maps/" + str(_script_start_time) + "/"
    if not os.path.isdir(_higher_level_maps_folder):
        os.makedirs(_higher_level_maps_folder)
#endregion folders

#region flask server
# Wrapper function to generate the thread function which runs the Flask server
def get_server_runner(server):
    def server_runner():
        return server.run(host="0.0.0.0", port=config.server_communication_port)
    return server_runner

# Sets up the various query/API endpoints/paths of the Flask server
def set_up_routes(server):
    @server.route("/")
    def home(): return "Flask server is up!"

    @server.route("/" + config.connectivity_test_path)
    def connectivity_test(): return config.server_connectivity_success_message
    
    @server.route("/" + config.register_side_ready_path)
    def register_side_ready():
        global _left_ready, _right_ready
        side = request.args.get("side").strip()
        if side == config.side_left:
            # Remember that we use lock because we're modifying global-variables
            #   which are accessed by multiple threads
            with _lock:
                _left_ready = True
            log("Left Pi is ready!")
            return config.get_server_received_side_ready_message(config.side_left)
        if side == config.side_right:
            with _lock:
                _right_ready = True
            log("Right Pi is ready!")
            return config.get_server_received_side_ready_message(config.side_right)
        log(config.get_invalid_side_message(side))
        return config.get_invalid_side_message(side)

    @server.route("/" + config.server_restart_path)
    def server_restart():
        log("Restart requested, restarting...")
        os.execl(sys.executable, sys.executable, *sys.argv)

    @server.route("/" + config.starting_info_path)
    def get_starting_info():
        global _left_ready, _right_ready, _image_transmission_starting_time
        if not (_left_ready and _right_ready):
            return config.server_not_ready_message
        log("Setting up starting info...")
        if not "_image_transmission_starting_time" in globals() or _image_transmission_starting_time is None:
            with _lock:
                _image_transmission_starting_time = datetime.datetime.now() + config.startup_delay
        log("")
        log("Both slaves are ready.")
        log("Starting to send data at: " + str(_image_transmission_starting_time)
            + ", in: " + str(_image_transmission_starting_time - datetime.datetime.now()))
        log("")
        return str(_image_transmission_starting_time)
#endregion flask server

# Wrapper function to generate the thread functions which listen over sockets for images from the Zero W's
def get_image_listener(side, check_interrupt):
    def image_listener():
        global _latest_left, _latest_right
        global _received_latest_left, _received_latest_right
        while True:
            log("Starting up server on " + side + " side...")

            server_socket = socket.socket()
            server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            server_socket.bind(('0.0.0.0', config.server_image_transfer_ports[side]))
            server_socket.listen(0)

            connection = server_socket.accept()[0].makefile("rb")
            try:
                while True:
                    start_time = datetime.datetime.now()
                    image_length = struct.unpack("<L", connection.read(struct.calcsize("<L")))[0]
                    if not image_length:
                        break
                    image_stream = io.BytesIO()
                    image_stream.write(connection.read(image_length))
                    image_stream.seek(0)
                    end_time = datetime.datetime.now()
                    log("Received image from " + side
                        + " with length " + str(image_length) + " over " + str(end_time - start_time))
                    
                    np_array = np.fromstring(image_stream.read(), np.uint8) # Transform image into numpy array
                    image = cv2.imdecode(np_array, cv2.IMREAD_UNCHANGED) # Transform np array into OpenCV image
                    cv2.imwrite(_images_folder + str(start_time) + "-" + side + ".jpg", image) # Save image to disk
                    with _lock:
                        if side == config.side_left:
                            _latest_left = image
                            _received_latest_left = True
                        if side == config.side_right:
                            _latest_right = image
                            _received_latest_right = True

                    check_interrupt()
            except:
                log("Error occurred on " + side + " listener; most likely, the slave disconnected.")
                connection.close()
                server_socket.close()
                check_interrupt()
            finally:
                connection.close()
                server_socket.close()
                check_interrupt()
            check_interrupt()
    return image_listener

#region image processing
# See https://docs.opencv.org/2.4/modules/core/doc/operations_on_arrays.html#flip
flip_image = lambda original: cv2.flip(original, -1)

# Load undistortion information from disk and register into global variables
def set_up_undistortion():
    global _mtx, _dist, _newcameramtx, _roi
    if not ("_mtx" in globals() and "_dist" in globals()) or _mtx is None or _dist is None:
        camera_distortion_info = np.load(_script_dir + "undistortion_info.npy", allow_pickle=True).tolist()
        _mtx, _dist = camera_distortion_info["mtx"], camera_distortion_info["dist"]
    w, h = config.image_resolution
    _newcameramtx, _roi = cv2.getOptimalNewCameraMatrix(_mtx, _dist, (w, h), 1, (w, h))

def undistort_image(original):
    global _mtx, _dist, _newcameramtx, _roi
    undistorted = cv2.undistort(original, _mtx, _dist, None, _newcameramtx)
    x, y, w, h = _roi
    return undistorted[y:y+h, x:x+w]

# Operation applied individually to each pixel when generating
#   higher-order 8x6 depth map
# The value of each pixel is squared to ensure that a small area of high intensity
#   in the original depth map will be well-represented in the higher-order map
higher_order_pixelwise_processor = lambda pix: pow(pix, 2)

# Calculates higher order depth map
def get_higher_order_depth_map(original):
    # Initialize empty array
    higher_order = np.ndarray(config.higher_order_depth_map_size, dtype=np.float32)
    height, width = higher_order.shape
    scale = int(float(config.higher_order_depth_map_size[0]) / height)

    # x and y are iteration indices in the higher order depth map
    for x in range(width):
        for y in range(height):
            sum = 0.0
            # i and j are iteration indices in the original depth map
            for j in range(y * scale, (y + 1) * scale):
                for i in range(x * scale, (x + 1) * scale):
                    # In preperation to take the average, process the current pixel using the algorithm
                    #   defined above and add to sum
                    sum += higher_order_pixelwise_processor(original[j][i])
            # Divide to take average and store in higher_order
            higher_order[y][x] = sum / (scale * scale) # scale*scale is width*height of the area of pixels
    return higher_order

# Wrapper function to generate the thread function which processes images into depth maps
def get_image_processor(check_interrupt):
    global _received_latest_left, _received_latest_right
    global _latest_left, _latest_right
    global _depth_map

    # StereoSGBM parameters adapted from http://timosam.com/python_opencv_depthimage
    #   and from https://jayrambhia.com/blog/disparity-mpas
    stereo = cv2.StereoSGBM_create(
        minDisparity=-64,
        numDisparities=192,
        blockSize=5,
        mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY,
        uniquenessRatio=15,
        P1=600,
        P2=2400,
        speckleWindowSize=150,
        speckleRange=2,
        preFilterCap=63
    )

    def image_processor():
        global _received_latest_left, _received_latest_right
        global _latest_left, _latest_right
        global _depth_map, _depth_map_ready

        if not os.path.isdir(_depth_maps_folder):
            os.makedirs(_depth_maps_folder)

        while True:
            # Wait until both left and right images are available and up-to-date
            if not (_received_latest_left and _received_latest_right) or _latest_left is None or _latest_right is None:
                check_interrupt();
                continue
            with _lock:
                received_latest_left, received_latest_right = False, False # Reset flags

            start_time = datetime.datetime.now()
            log("Starting to process images")

            # Copy images into local variables so we can free up global variables
            left_image, right_image = np.copy(_latest_left), np.copy(_latest_right)
            with _lock:
                _latest_left, _latest_right = None, None
            
            log("Undistorting, scaling, and flipping images...")
            left_image, right_image = undistort_image(left_image), undistort_image(right_image)
            for i in range(config.image_scale_down_factor):
                # OpenCV's pyrDown scales down an image to 1/4 original area (halves width and length)
                # Smaller image dimensions => faster computing of depth map
                left_image, right_image = cv2.pyrDown(left_image), cv2.pyrDown(right_image)
            # On the device the cameras were installed upside down so we flip the images in code :)
            left_image, right_image = flip_image(left_image), flip_image(right_image)

            log("Computing depth map...")
            # Convert the images to grayscale before processing depth information
            # picamera will often try to automatically adjust colour balances to match scenery
            # This isn't necessarily consistent between cameras or between images from same cam
            # Converting to grayscale helps reduce the chaos inherent to image capture
            left_image = cv2.cvtColor(left_image, cv2.COLOR_BGR2GRAY)
            right_image = cv2.cvtColor(right_image, cv2.COLOR_BGR2GRAY)
            _depth_map = stereo.compute(left_image, right_image).astype(np.float32)
            check_interrupt()

            end_time = datetime.datetime.now()
            log("Computed depth map over time interval " + str(end_time - start_time))
            cv2.imwrite(_depth_maps_folder + str(start_time) + ".jpg", _depth_map) # Store map in file
            _depth_map_ready = True
            check_interrupt()
    return image_processor
#endregion image processing

# Wrapper function to generate the thread function which generates higher order
#   depth maps and manages conversion of depth info into audio info
def get_audio_processor(check_interrupt):
    global _depth_map_ready, _depth_map
    global _audio_source_data, _lock

    def audio_processor():
        global _depth_map_ready, _depth_map
        global _audio_source_data, _lock
        while True:
            # Wait until depth map is available and ready
            if (not _depth_map_ready) or _depth_map is None:
                check_interrupt();
                continue
            depth_map = np.copy(_depth_map) # Copy to free up global variables
            with _lock:
                _depth_map_ready, _depth_map = False, None # Reset global variables
            
            log("Computing higher order depth map...")
            ho_map = get_higher_order_depth_map(depth_map)
            cv2.imwrite(_higher_level_maps_folder + str(datetime.datetime.now()) + ".jpg", ho_map)
            check_interrupt()

            log("Updating audio volumes...")
            with _lock:
                # Copy the higher order map directly into the audio source data matrix
                # The positions of data in the HO map are where we want them in the audio data
                #   and the volumes can be multiplied by some constant during playback
                _audio_source_data = ho_map.copy()
            check_interrupt()
    return audio_processor

#region audio player
# Set up the single audio source and initialize using  some provided sound file
def set_up_source(sound_file=config.solid_note_file):
    global _audio_source_data, _audio_source
    sound = openal.Buffer(openal.WaveFile(_script_dir + sound_file))
    _audio_source = openal.Source(sound)
    _audio_source_data = np.zeros(config.higher_order_depth_map_size)

 # Wrapper function to generate the thread function which plays the audio
def get_audio_player(check_interrupt):
    global _audio_source_data, _audio_source
    def audio_player():
        global _audio_source_data, _audio_source
        h, w = config.higher_order_depth_map_size
        # Start playing the audio
        _audio_source.play()
        while True:
            with _lock:
                source_data = np.copy(_audio_source_data) # Copy to ensure edit-ibility of global var
            for x in range(w): # Left to right
                for y in reversed(range(h)): # Top to bottom (hence reversed)
                    _audio_source.set_position((
                        # Respective offsets of -3.5 and -2.5 for x- and y-positions
                        #   move the sound positions to a centered position. For example, x-pos
                        #   in the HO depth map is in [0, 7]. Transformation by -3.5
                        #   forces it into [-3.5, 3.5] which is centered around x=0
                        config.sound_source_displacement_scale * (x - 3.5),
                        config.sound_source_displacement_scale * (y - 2.5),
                        config.sound_zpos
                    ))
                    # Simply set the volume of the sound source to the corresponding HO intensity * a constant
                    _audio_source.set_gain(_audio_source_data[y][x] * config.sound_volume_multiplier)
                    time.sleep(0.1) # Experimentally gives enough time to process without wasting time
            time.sleep(0.3) # Similarly determined

            check_interrupt()
    return audio_player
#endregion audio player

def run_master():
    with util.InterruptableRegion() as interrupt_manager:
        check_interrupt = util.get_interruption_checker(interrupt_manager, on_interrupt)
        set_up_log_folder()
        set_up_images_folder()
        set_up_depth_maps_folder()
        set_up_higher_level_maps_folder()

        log("Master script starting up")
        
        global _script_start_time
        _script_start_time = datetime.datetime.now()

        log("Setting up Flask communication server...")
        server = Flask(__name__)
        set_up_routes(server)

        log("Setting up image undistortion algorithm...")
        set_up_undistortion()

        log("Setting up virtual audio sources...")
        set_up_source(sound_file=config.solid_note_file)
        
        log("Setting up and starting threads...")
        make_thread("server_thread", get_server_runner(server), True)
        make_thread("left_receiver_thread", get_image_listener(config.side_left, check_interrupt), True)
        make_thread("right_receiver_thread", get_image_listener(config.side_right, check_interrupt), True)
        make_thread("image_processor_thread", get_image_processor(check_interrupt), True)
        make_thread("audio_processor_thread", get_audio_processor(check_interrupt), True)
        make_thread("audio_player_thread", get_audio_player(check_interrupt), True)

if __name__ == "__main__":
    run_master()