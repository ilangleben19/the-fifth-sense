###
# General configuration file
# All constants needed by both slaves and masters are stored here
###


import datetime, urllib.parse

# Side info
side_left = "left"
side_right = "right"
sides = [side_left, side_right]

# Raw server communication info
server_ip_address_for_slaves = "10.42.0.1"
server_communication_port = 8081
server_address_for_slaves = "http://" + server_ip_address_for_slaves + ":" + str(server_communication_port)
server_image_transfer_ports = {side_left: 8082, side_right: 8083}

# Query/API paths for Flask server
connectivity_test_path = "connectivity_test"
register_side_ready_path = "register_side_ready"
starting_info_path = "get_starting_info"
server_restart_path = "server_restart"

# URL's for slaves to access Flask server query/API paths
connectivity_test_url = urllib.parse.urljoin(server_address_for_slaves, connectivity_test_path)
get_register_side_ready_url = lambda side: urllib.parse.urljoin(server_address_for_slaves, register_side_ready_path) + "?side=" + side
starting_info_url = urllib.parse.urljoin(server_address_for_slaves, starting_info_path)
server_restart_url = urllib.parse.urljoin(server_address_for_slaves, server_restart_path)

# Standardized messages
server_connectivity_success_message = "Ready!"
get_server_received_side_ready_message = lambda side: "Ok; " + side.strip() + " ready!"
server_not_ready_message = "Not ready!"
get_invalid_side_message = lambda side: "'" + side + "' is not a valid side :("

# Image transmission and processing information
startup_delay = datetime.timedelta(seconds=5)
image_send_time_interval = datetime.timedelta(seconds=4)
image_resolution = (1024, 768)
image_scale_down_factor = 2
higher_order_depth_map_size = (6, 8)

# Sound file and virtual 3D source information
sound_dir = "sounds/"
solid_note_file = urllib.parse.urljoin(sound_dir, "c2.wav")
stayin_alive_file = urllib.parse.urljoin(sound_dir, "stayin_alive.wav")
sound_source_displacement_scale = 1
sound_zpos = 5
sound_volume_multiplier = 10.0